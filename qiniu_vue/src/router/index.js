import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Upload from '@/components/Upload'
import Vod from '@/components/Vod'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'HelloWorld', component: HelloWorld },
    { path: '/upload', name: 'Upload', component: Upload },
    { path: '/vod', name: 'Vod', component: Vod },
  ]
})
